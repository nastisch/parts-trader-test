Feature: Buy a product via website
I want to buy a clothing item online

Scenario: Adding 2 cheapest green summer dresses in M size to cart
Given I navigate to the website 
When I search for 'Printed Summer Dress'
And I sort the items to display the cheapest first
And I select the cheapest dress
And I change the Quantity from 1 to 2
And I change the Size from S to M
And I change the colour from Yellow to Green
And I select 'Add to cart'
When I select 'Proceed to checkout'
And I verify that the Total price equals '$34.80'
Then I can take and save a screenshot of the cart