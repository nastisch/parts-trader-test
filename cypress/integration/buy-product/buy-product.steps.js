import BuyProduct from './buy-product.po';

Given('I navigate to the website', () => {
  BuyProduct.goToPage().url().should('match', /automationpractice.com/);
});

When('I search for {string}', (dress_type) => {
  BuyProduct.searchBox().type(`${dress_type}{enter}`);
});

And('I sort the items to display the cheapest first', () => {
  BuyProduct.sortDropdown().select('Price: Lowest first');
});

And('I select the cheapest dress', () => {
  BuyProduct.firstSearchResult().click();
});

And('I change the Quantity from 1 to 2', () => {
  BuyProduct.quantityPlusButton().click();
});

And('I change the Size from S to M', () => {
  BuyProduct.sizeDropdown().select('M');
});

And('I change the colour from Yellow to Green', () => {
  BuyProduct.greenColourIcon().should('have.attr', 'name', 'Green').click();
});

And('I select \'Add to cart\'', () => {
  BuyProduct.addToCartButton().click();
});

When('I select \'Proceed to checkout\'', () => {
  BuyProduct.proceedToCheckoutButton().click();
});

And('I verify that the Total price equals {string}', (total_price) => {
  BuyProduct.totalPrice().should('contain', total_price).and('be.visible');
});

Then('I can take and save a screenshot of the cart', () => {
  cy.get('#center_column').screenshot();
});
