export default class BuyProduct {
  static goToPage() {
    return cy.visit('http://automationpractice.com/index.php');
  }

  static searchBox() {
    return cy.get('#searchbox').children('input[name="search_query"]');
  }

  static sortDropdown() {
    return cy.get('#productsSortForm').find('.selectProductSort');
  }

  static firstSearchResult() {
    return cy.get('.product_list').children('.first-in-line').contains('More');
  }

  static quantityPlusButton() {
    return cy.get('#quantity_wanted_p').find('.button-plus');
  }

  static sizeDropdown() {
    return cy.get('#attributes').find('.attribute_select');
  }

  static greenColourIcon() {
    return cy.get('#color_to_pick_list').find('.color_pick').first();
  }

  static addToCartButton() {
    return cy.get('#add_to_cart').children('button[type="submit"]');
  }

  static proceedToCheckoutButton() {
    return cy.get('.layer_cart_cart').find('.button').contains('Proceed to checkout');
  }

  static totalPrice() {
    return cy.get('#cart_summary').find('tfoot tr:last td:last');
  }
}
