# Introduction

Automation tests written using Cypress as a part of PartsTrader technical test.
Page Object Model is used as a design pattern.

## Getting started

1. Install Visual Studio Code
1. (Optional) Install Visual Studio Code Extensions
    - [alexkrechik.cucumberautocomplete](https://marketplace.visualstudio.com/items?itemName=alexkrechik.cucumberautocomplete)
    - [streetsidesoftware.code-spell-checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
    - [shevtsov.vscode-cy-helper](https://marketplace.visualstudio.com/items?itemName=shevtsov.vscode-cy-helper#1-open-cypress-custom-command-definition)
1. Install Node.js

## How to run the tests via Cypress UI

1. Clone this repository to your local machine
1. Ensure that Node or NPM is installed by running `npm -v`
1. Install project dependencies by running `npm ci`
1. Open Cypress UI by running `npm run cypress:ui`